===================================================================================
Premium WordPress Website
===================================================================================


-----------------------------------------------------------------------------------
Seite einrichten in local-App


-----------------------------------------------------------------------------------
understrap- und understrap-child-theme installieren


-----------------------------------------------------------------------------------
Sprache und Zeitzone in WP einstellen


-----------------------------------------------------------------------------------
Repository in Bitbucket.com einrichten in Projektordner 'Kundenprojekte'
- kein .gitnore? nötig


-----------------------------------------------------------------------------------
Kommandozeile cmd+shift+P in Visual Code Editor öffnen
GPM: Refresh Git Project
GPM: Open Git Project


-----------------------------------------------------------------------------------
Git-Repository einrichten:
Master Push

-----------------------------------------------------------------------------------
node_modules anpassen
package.json definiert:
- welche Scripts aufgerufen werden können
- was in node_modules geladen wird.
- 'css-postcss' von Script 'css' entfernt

-----------------------------------------------------------------------------------
Child-Theme anpassen
- Name
- Version
- Evtl. Funktionen
- Evtl. CSS- und JS-Imports
- Evtl. WooCommerce deaktivieren


-----------------------------------------------------------------------------------
Push changes to Git: Default Theme Settings


-----------------------------------------------------------------------------------
npm run watch
oder
npm run watch -bs


-----------------------------------------------------------------------------------
WordPress Einstellungen
- Einstellungen Allgemein (Untertitel; Sprache Schweiz, Du/Sie; Zeitzone)
- Einstellungen Diskussionen (Standardeinstellungen für Beiträge entfernen, Avataranzeige entfernen)
- Design Customizer durchgehen. Logo und Favicon einfügen.
- Sprache de_CH


-----------------------------------------------------------------------------------
Template Vorbereiten
- footer.php -> Footer-Line from understrap entfernen
- src/sass/child-theme.scss -> WooCommerce auskommentieren
- src/sass/child-theme.scss -> Font Awesome Icon font auskommentieren


-----------------------------------------------------------------------------------
functions.php
- Google Font laden für Frontend

-----------------------------------------------------------------------------------
src/sass/child-theme.scss
- Google-Font einbinden
- Neue Theme-Sass-Dateien einbinden


-----------------------------------------------------------------------------------
src/sass/theme/_child_theme_variables.scss
- Brandfarben definieren
- Breakpoints definieren

-----------------------------------------------------------------------------------
src/sass/theme/_child_theme_header.scss
src/sass/theme/_child_theme_footer.scss
- Layout-Styles definieren

-----------------------------------------------------------------------------------
src/sass/theme/_child_theme_typography.scss
- Schriftstile definieren. Zum Beispiel .text-uppercase erweitern mit letter-spacing

-----------------------------------------------------------------------------------
src/sass/custom-editor-style.scss
- Einzelne Styles für Backend definieren welche nicht über Blocks gesteuert werden


-----------------------------------------------------------------------------------
Block-Einstellungen
inc/block-editor.php
    - von perent-Theme kopiert
    - Styles und Google Font laden für und Frontend
/node_modules/postcss-understrap-palette-generator/index.js

-----------------------------------------------------------------------------------
Fixes Gutenberg's row block margin in use with bootstrap row -> col classes.
/src/sass/custom-editor-style.scss
/src/sass/theme/_child_theme_blocks.scss


===================================================================================
Blocks definieren mit ACF
1. Block erstellen in separatem File: /inc/acf-blocks/blocktitle.php
2. Block importieren in /inc/acf-blocks.php
3. Block-Template-Part erstellen in /template-parts/blocks/blocktitle/blocktitle.php
    3.1 Styles definieren, welche im Block erstellt werden können (z.B. background-color)
4. SASS erstellen in /src/sass/theme/blocks/_blocktitle.scss
5. Integrieren in /src/sass/theme/_child_theme_blocks.sass (wird in Front- und Backend-Stylesheet integriert)

+ FontAwesome integriert
+ mächtigere Felder
+ Eigene Settings-Seiten
- Keine Auswahl für Inspector Felder

===================================================================================
Blocks definieren mit Genesis Blocks Builder
1. Block erstellen in Backend
3. Block-Template-Part erstellen in Backend
4. SASS erstellen in /src/sass/theme/blocks/_blocktitle.scss
5. Integrieren in /src/sass/theme/_child_theme_blocks.sass (wird in Front- und Backend-Stylesheet integriert)

+ On the fly Kategorien
+ Felder in Inhalt oder Inspector möglich
- Zusätzliches Plugin
- Zusätzliche Kosten

***********************************************************************************
Default Plugins
- Nested Pages (Menu -> Kommentare entfernen)
- Language Fallback
- Advanced Custom Fields Pro
- Genesis Custom Blocks
- SVG Support
- Fontawesome (Kit)
- WP Rocket
- WPS Hide Login  (Evtl. mit Security-Plugin machbar)

***********************************************************************************
Entwicklungsplugins
- Query Monitor
- FakerPress

***********************************************************************************
Entwicklungsplugins
- EditorsKit (Frontend CSS https://schwarzpunkt.local/wp-content/plugins/block-options/build/style.build.css?ver=1.31.6)
- Advanced Custom Fields: Font Awesome