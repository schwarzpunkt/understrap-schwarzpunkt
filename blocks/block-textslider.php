<?php
 // Example Repeater Field
$activeClass = 'active';
if ( block_rows( 'slides' ) ):
    echo '<div id="textslider" class="carousel slide alignfull" data-bs-ride="carousel">';
    echo '<div class="carousel-inner">';
    while ( block_rows( 'slides' ) ) :
        block_row( 'slides' );
        echo '<div class="carousel-item ' . $activeClass . '" data-bs-interval="4000">';
        echo '<h2 class="entry-title">';
        block_sub_field( 'text' );
        echo '</h2>';
        echo '</div>';
        $activeClass = '';
    endwhile;
    echo '</div>';
    echo '</div>';
endif;

reset_block_rows( 'slides' );