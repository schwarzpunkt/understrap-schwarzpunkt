<?php

/**
 * Button Block Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */



// Load values and assign defaults.
$text = get_field('text') ?: 'Ihr Buttontext...';
$link = get_field('link') ?: '';
$style = get_field('style') ?: 'primary';
$size = get_field('size') ?: 'md';
$align = get_field('align') ?: 'left';
if ( $align === 'block') {
    $btn_block_class = ' btn-block';
    $btn_align = false;
}
else {
    $btn_align = true;
    $btn_block_class = '';
}

// Create id attribute allowing for custom "anchor" value.
$id = 'button-' . $block['id'];
if( !empty($block['anchor']) ) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'btn btn-' . $size . ' btn-' . $style . $btn_block_class;
if( !empty($block['className']) ) {
    $className .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
    $className .= ' align' . $block['align'];
}

?>
<p class='text-<?=$align?>'>
    <button id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>">
    <?php echo $text; ?>
    <style type="text/css">
            #<?php echo $id; ?> {
            }
            </style>
    </button>
</p>