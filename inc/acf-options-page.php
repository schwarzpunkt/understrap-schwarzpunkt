<?php
    // ACF Options Page

    if( function_exists('acf_add_options_page') ) {
	
        acf_add_options_page(array(
            'page_title' 	=> 'Allgemeine Einstellungen',
            'menu_title'	=> 'Allgemein',
            'menu_slug' 	=> 'theme-general-settings',
            'capability'	=> 'edit_posts',
            'position'      => 1,
            'icon_url'		=> 'dashicons-layout',
            'redirect'      => true
        ));

        acf_add_options_sub_page([
            'page_title' => 'Kontaktangaben',
            'menu_title' => 'Kontaktangaben',
            'menu_slug' => 'contact',
            'parent_slug' => 'theme-general-settings',
            'update_button' => __('Aktualisieren', 'acf'),
            'updated_message' => __("Kontaktangaben aktualisiert.", 'acf'),
        ]);
        
        acf_add_options_sub_page(array(
            'page_title' => 'Statuszeile',
            'menu_title' => 'Statuszeile',
            'menu_slug' => 'status_row',
            'parent_slug' => 'theme-general-settings',
            'update_button' => __('Aktualisieren', 'acf'),
            'updated_message' => __("Statuszeile aktualisiert.", 'acf'),
        ));

        acf_add_options_sub_page([
            'page_title' => 'Social Media',
            'menu_title' => 'Social Media',
            'menu_slug' => 'social_media',
            'parent_slug' => 'theme-general-settings',
            'update_button' => __('Aktualisieren', 'acf'),
            'updated_message' => __("Social Media aktualisiert.", 'acf'),
        ]);
  
        acf_add_options_sub_page([
            'page_title' => 'Hinweis',
            'menu_title' => 'Hinweis',
            'menu_slug' => 'notice',
            'parent_slug' => 'theme-general-settings',
        ]);        

        
    } 