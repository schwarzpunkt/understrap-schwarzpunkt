<?php 
// register a button block.
acf_register_block_type(array(
    'name'              => 'button',
    'title'             => __('Button'),
    'description'       => __('Eine einfache Schaltfläche.'),
    'render_template'   => 'template-parts/blocks/button/button.php',
    'category'          => 'brand',
    'icon'              => 'button',
    'keywords'          => array( 'button', 'link', 'schaltfläche' ),
    'mode'              => 'auto',
    'supports'          => array( 'align' => false, 'mode' => false ),

));