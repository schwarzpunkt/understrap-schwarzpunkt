<?php
/**
 * Block editor (gutenberg) specific functionality
 *
 * @package Understrap
 */


add_action( 'after_setup_theme', 'understrap_block_editor_setup' );

if ( ! function_exists( 'understrap_block_editor_setup' ) ) {

	/**
	 * Sets up our default theme support for the WordPress block editor.
	 *
	 */
	function understrap_block_editor_setup() {

		// Add support for the block editor stylesheet.
		add_theme_support( 'editor-styles' );

		// Enqueue editor styles.
		add_editor_style( array( '/css/custom-editor-style.min.css', 'https://fonts.googleapis.com/css2?family=Lato:ital,wght@0,400;0,700;0,900;1,400&family=Oswald:wght@500&display=swap' ) );

		// Add support for wide alignment.
		add_theme_support( 'align-wide' );

		// Make media embeds responsive.
		//add_theme_support( 'responsive-embeds' );

		// Adds support for editor color palette.
		add_theme_support(
			'editor-color-palette',
			array(
				array(
					'name'  => __( 'Schriftfarbe', 'schwarzpunkt-understrap' ), // Called “Link Color” in the Customizer options. Renamed because “Link Color” implies it can only be used for links.
					'slug'  => 'dark',
					'color' => '#212121',
				),
				array(
					'name'  => __( 'Brandfarbe 1', 'schwarzpunkt-understrap' ), // Called “Link Color” in the Customizer options. Renamed because “Link Color” implies it can only be used for links.
					'slug'  => 'primary',
					'color' => '#D2DC00',
				),
				array(
					'name'  => __( 'Brandfarbe 2', 'schwarzpunkt-understrap' ), // Called “Link Color” in the Customizer options. Renamed because “Link Color” implies it can only be used for links.
					'slug'  => 'secondary',
					'color' => '#2688A5',
				),
				array(
					'name'  => __( 'Weiss', 'schwarzpunkt-understrap' ), // Called “Link Color” in the Customizer options. Renamed because “Link Color” implies it can only be used for links.
					'slug'  => 'white',
					'color' => '#FFFFFF',
				),
				array(
					'name'  => __( 'Hell', 'schwarzpunkt-understrap' ), // Called “Link Color” in the Customizer options. Renamed because “Link Color” implies it can only be used for links.
					'slug'  => 'light',
					'color' => '#E0DED8',
				),
				array(
					'name'  => __( 'Schwarz', 'schwarzpunkt-understrap' ), // Called “Link Color” in the Customizer options. Renamed because “Link Color” implies it can only be used for links.
					'slug'  => 'black',
					'color' => '#000000',
				),
			)
		);
	}
}

	/**
	 * Checks for our JSON file of color values. If exists, creates a color palette array.
	 *
	 * @return array|bool
	 */
	function understrap_generate_color_palette() {
		return false;
	}
