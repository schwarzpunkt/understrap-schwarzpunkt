<?php

function sp_block_category( $categories, $post ) {
	return array_merge(
		$categories,
		array(
			array(
				'slug' => 'brand',
				'title' => 'Brand',
			),
		)
	);
}
add_filter( 'block_categories', 'sp_block_category', 10, 2);


add_action('acf/init', 'sp_acf_init_block_types');
function sp_acf_init_block_types() {

    // Check function exists.
    if( function_exists('acf_register_block_type') ) {

        include 'acf-blocks/button.php';
        //include 'acf-blocks/subtitle.php';
        //include 'acf-blocks/theme/pagelinks.php';

    }
}